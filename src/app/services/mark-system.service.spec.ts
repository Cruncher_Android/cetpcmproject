import { TestBed } from '@angular/core/testing';

import { MarkSystemService } from './mark-system.service';

describe('MarkSystemService', () => {
  let service: MarkSystemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MarkSystemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
