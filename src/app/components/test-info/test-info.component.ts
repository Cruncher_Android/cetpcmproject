import { PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Component, OnInit, Input } from '@angular/core';
import { ChapterListComponent } from '../chapter-list/chapter-list.component';

@Component({
  selector: 'app-test-info',
  templateUrl: './test-info.component.html',
  styleUrls: ['./test-info.component.scss'],
})
export class TestInfoComponent implements OnInit {

  @Input() chapters = [];
  @Input() subjectName: string;
  @Input() testMode: string;
  @Input() cntManual: {
    cntEasy: number,
    cntMedium: number,
    cntHard: number,
    cntNum: number,
    cntTheory: number,
    cntNonAtt: number,
    cntAtt: number,
    cntPrevAsk: number
  };

  @Input() settings: {
    time: number,
    correctMarks: number,
    inCorrectMarks: number,
    questionCount: number,
  } 
  @Input() questionCount;
  @Input() questions = [];
  chapterNames: {
    chapterId: number,
    chapterName: string,
    subjectId: number
  }[] = [];
  withQuestionCount: boolean = false;
  manualType: string = '';
  page: string = '';
 
  
  constructor(private storage: Storage,
              private popOverController: PopoverController) { }

  ngOnInit() {
  
    // console.log('chapters', this.chapters);
    // console.log('subjectName', this.subjectName);
    // console.log('questions', this.questions);
    // console.log('questionCount', this.questionCount);
    // console.log('testMode', this.testMode);
    // console.log('cntManual', this.cntManual);
    // console.log('settings', this.settings);
    
    this.storage.ready().then(ready => {
      if(ready) {
        this.storage.get('chapterNames').then(result => {
          this.chapterNames = result;
          // console.log(this.chapterNames);

          if(this.questions.length > 1) {
            this.withQuestionCount = true;
            let chapterNamesWithQueCount = [];
            this.chapterNames.map(chapter => {
     
               chapterNamesWithQueCount.push({
                 chapterId: chapter.chapterId,
                 chapterName: chapter.chapterName,
                 subjectId: chapter.subjectId,
                 questionCount: this.questions.find(({chapterId}) => chapterId == chapter.chapterId).questionCount
               })
              })
              this.chapterNames = chapterNamesWithQueCount;
              // console.log(this.chapterNames);
          }
        
        })
      }
    })
    this.storage.get('manualType').then(data => {
      this.manualType = data;
    })

    this.storage.get('page').then(data => {
      this.page = data;
    })
  
  }

  async showChapters(event) {
    const popover = await this.popOverController.create({
      component: ChapterListComponent,
      animated: true,
      cssClass: 'myPopOver1',
      event: event,
      componentProps: {
        chapterNames: this.chapterNames,
        subjectName: this.subjectName,
        withQuestionCount: this.withQuestionCount
      }
    });
    await popover.present();
    popover.onDidDismiss().then(_ => {
      this.storage.set('changeSettingsPopOver', false);
    })
  }

  onClose() {
    this.popOverController.dismiss();
  }

}
