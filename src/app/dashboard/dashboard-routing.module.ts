import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardPage } from './dashboard.page';

const routes: Routes = [

  {
    path: '',
    component: DashboardPage,
    children: [
      {
        path: 'home',
        loadChildren: () => import('../home/home.module').then(m => m.HomePageModule)
      },
      {
        path: 'unit-test',
        loadChildren: () => import('../home/unit-test/unit-test.module').then(m => m.UnitTestPageModule)
      },
      {
        path: 'bookmark',
        loadChildren: () => import('../home/bookmark/bookmark.module').then(m => m.BookmarkPageModule)
      },
      {
        path: 'performance-graph',
        loadChildren: () => import('../home/performance-graph/performance-graph.module').then( m => m.PerformanceGraphPageModule)
      },
      {
        path: 'career-notes',
        loadChildren: () => import('../home/career-notes/career-notes.module').then( m => m.CareerNotesPageModule)
      },
      {
        path: 'test-series',
        loadChildren: () => import('../home/test-series/test-series.module').then( m => m.TestSeriesPageModule)
      },
      {
        path: 'view-test',
        loadChildren: () => import('../home/view-test/view-test.module').then( m => m.ViewTestPageModule)
      },
      {
        path: 'question-paper',
        loadChildren: () => import('../home/question-paper/question-paper.module').then( m => m.QuestionPaperPageModule)
      },
      {
        path: 'about-us',
        loadChildren: () => import('../home/about-us/about-us.module').then( m => m.AboutUsPageModule)
      },
      {
        path: 'profile',
        loadChildren: () => import('../home/profile/profile.module').then( m => m.ProfilePageModule)
      },
      {
        path: 'notes',
        loadChildren: () => import('../home/notes/notes.module').then( m => m.NotesPageModule)
      },
      {
        path: 'performance-graph',
        loadChildren: () => import('../home/performance-graph/performance-graph.module').then( m => m.PerformanceGraphPageModule)
      },
      {
        path: 'settings',
        loadChildren: () => import('../home/settings/settings.module').then( m => m.SettingsPageModule)
      }
    ]
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardPageRoutingModule {}
