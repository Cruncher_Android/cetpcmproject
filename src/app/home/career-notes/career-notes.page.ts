import { Component, OnInit } from "@angular/core";
import { Storage } from "@ionic/storage";
import { AlertController } from "@ionic/angular";

@Component({
  selector: "app-career-notes",
  templateUrl: "./career-notes.page.html",
  styleUrls: ["./career-notes.page.scss"],
})
export class CareerNotesPage implements OnInit {
  careers = [
    "Accessory Designer",
    "Actor",
    "Actuary",
    "Advertising Executive",
    "Aeronautical Engineer",
    "Aerospace/Aeronautical Engineering",
    "Agricultural Engineer",
    "Agricultural Scientist",
    "Air Force Officer",
    "Air Hostess",
    "Aircraft Maintenance Engineer",
    "Anchor",
    "Animation",
    "Anthropologist",
    "Aqua-culturist",
    "Archaeologist/Historian",
    "Army Officer",
    "Astronomer",
    "Automobile Engineer",
    "Ayurved Physician",
    "Banking Ombudsman",
    "Biochemist",
    "Bioinformatics Scientist",
    "Biomedical Engineer",
    "BIOTECHNOLOGIST",
    "Beautician",
    "Cartoonist",
    "Ceramic Engineer",
    "Certified Financial Analyst",
    "Chartered Accountant",
    "Chemical Engineer",
    "Civil Engineers",
    "Civil Servant (IAS/PCS)",
    "Community Development Worker",
    "Company Secretary",
    "Computer Engineers",
    "Dairy Scientist",
    "Dentist",
    "Doctor",
    "Economist",
    "Electrical Engineers",
    "Electronic Engineers",
    "Energy Engineers",
    "Environmental Engineers",
    "Environmentalist",
    "Fashion Designer",
    "Film/ TV Director",
    "Fishery Inspector",
    "Food Technologist",
    "Forensic Science Technician",
    "Forester",
    "Gem & Jewellery Maker",
    "Geneticist",
    "Geologist",
    "Home Science Professional",
    "Homeopath",
    "Horticul Turist",
    "Hospital/ Health Manager",
    "Industrial Engineers",
    "Instrumentation (Production) Engineers",
    "Insurance Agent",
    "Interior Designer",
    "Landscape Architect",
    "Lawyer",
    "Library Assistant",
    "Marine Engineers",
    "Marketing Manager",
    "Mechanical engineers",
    "Medical Technologist",
    "Merchant Navy Officer",
    "Metallurgical and Materials Engineers",
    "Meteorologist",
    "Microelectronics engineers",
    "Mining Engineer",
    "Nuclear engineers",
    "Nurse",
    "Oceanographer",
    "Personnel Manager",
    "Pharmacist",
    "Photographer",
    "Physiotherapist",
    "Pilot",
    "Prosthetic & Orthotic Engineer",
    "Protective Services Personnel",
    "Psychologist",
    "Public Relations officer",
    "Retail Manager",
    "Rural Manager",
    "Sales Manager",
    "Social Worker",
    "Sociologist",
    "Speech Therapist",
    "Stock Broker",
    "Teacher",
    "Telecommunication Engineers",
    "Texttile Designer",
    "Textile Technologist",
    "Travel Counsellor",
    "Veterinarian",
    "Visual Communication Specialist",
  ];

  careerNotes: string = "./career-notes.page.html";

  careerListPage: boolean = true;
  careerInfoPage: boolean = false;
  career: number;

  loggedInType: string = "";

  constructor(
    private storage: Storage,
    private alertController: AlertController
  ) {
    // console.log("career page loaded");
  }

  ngOnInit() {
    this.storage.get("userLoggedIn").then((data) => {
      this.loggedInType = data;
    });
  }

  onCareerClick(career) {
    if (career > 1 && this.loggedInType == "demo") {
      this.createAlert('Please contact your distributer for complete access');
      return
    }
    this.career = career;
    this.careerListPage = false;
    this.careerInfoPage = true;
  }

  onBackClick() {
    this.careerListPage = true;
    this.careerInfoPage = false;
  }

  async createAlert(message) {
    const alert = await this.alertController.create({
      subHeader: message,
      cssClass: "alert-title",
      animated: true,
      buttons: [
        {
          role: "cancel",
          text: "Ok",
        },
      ],
    });
    await alert.present();
  }
}
